import React from 'react'

import styles from './styles.module.scss'

export const GameOver = () => {
    return (
        <div className={styles.container}>
            {'GameOver'}
        </div>
    )
}
