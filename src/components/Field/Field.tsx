import React, { useEffect, useRef } from "react"
import { useAppSelector, useAppDispatch } from '../../state/hooks'

import { StartButton } from '../StartButton/StartButton'
import { GameOver } from '../GameOver/GameOver'
import { PositionHelper } from '../PositionHelper/PositionHelper'

import { addFigure, rotate as rotateFigure, moveLeft, moveRight, moveDown, removeFigure, clearNewFigure, startGame, initField } from "../../state/actions"
import { getRotatedFigure, rotatedLength } from "../../utils"
import { useColorComponent } from "../../hooks"

import { FIGURES, COLORS } from "../../constants"

import styles from './styles.module.scss'

export const Field = () => {
    const animation = useRef(null)

    const field = useAppSelector(state => state.field.field)
    const cellWidth = useAppSelector(state => state.field.cellWidth)
    const cellHeight = useAppSelector(state => state.field.cellHeight)
    const dimentions = useAppSelector(state => state.field.dimentions)
    const rotate = useAppSelector(state => state.field.rotate)
    const x = useAppSelector(state => state.field.x)
    const y = useAppSelector(state => state.field.y)
    const colorNumber = useAppSelector(state => state.field.color)
    const currentFigure = useAppSelector(state => state.field.figure)
    const gameOver = useAppSelector(state => state.field.gameOver)
    const gameStart = useAppSelector(state => state.field.gameStart)
    const newFigureFlag = useAppSelector(state => state.field.newFigureFlag)
    const speed = useAppSelector(state => state.field.speed)
    
    const dispatch = useAppDispatch()

    const figure = { current: null }
    figure.current = getRotatedFigure(FIGURES[currentFigure], rotate)

    // Запоминаем длину и ширину для удаления фигуры
    let figureWidth = figure.current.length,
        figureHeight = figure.current[0].length
    let handleTimeTickInProgress = false

    const handleTimeTick = () => {
        handleTimeTickInProgress = true

        if (!gameOver && gameStart) {
            dispatch(moveDown(figure.current, false))
        }

        animation.current = null
    }

    useEffect(() => {
        if (gameStart && !gameOver) {
            document.addEventListener('keydown', handleKeyPress, false)
            //console.log('Effect add figure---', 'x', x, 'y', y, 'rotate', rotate);
            
            dispatch(addFigure(figure.current, x, y, colorNumber))
            dispatch(clearNewFigure())
        
            if (animation.current === null) animation.current = setTimeout(handleTimeTick, speed)
        }

        return () => {
            if (gameStart && !gameOver) {
                clearTimeout(animation.current)
                animation.current = null
                //console.log('Effect remove figure---', 'x', x, 'y', y, 'rotate', rotate);
                dispatch(removeFigure(figure.current, x, y, figureWidth, figureHeight))
                document.removeEventListener("keydown", handleKeyPress, false)
            }
        }
    }, [x, y, rotate, gameStart, gameOver, newFigureFlag])

    const handleKeyPress = (e) => {
        if (!handleTimeTickInProgress) {
            switch (e.key) {
                case 'ArrowUp':
                case 'Up': {
                    //console.debug('--------------------------------------------------------')
                    //console.debug('Remove --------------------------------------')
                    dispatch(removeFigure(figure.current, x, y, figureWidth, figureHeight))
                    //console.debug('Rotate --------------------------------------')
                    dispatch(rotateFigure(rotatedLength(figure.current)))
                    //console.debug('--------------------------------------------------------')
                    break
                }
                case 'ArrowDown':
                case 'Down': {
                    dispatch(moveDown(figure.current, false))
                    break
                }
                case 'ArrowLeft':
                case 'Left': {
                    dispatch(moveLeft())
                    break
                }
                case 'ArrowRight':
                case 'Right': {
                    dispatch(moveRight(figure.current.length))
                    break
                }
                case 'Space':
                case ' ': {
                    console.log('Space -----');
                    //console.debug('--------------------------------------------------------')
                    //console.debug('Remove --------------------------------------')
                    dispatch(removeFigure(figure.current, x, y, figureWidth, figureHeight))
                    //console.debug('MoveDown --------------------------------------')
                    dispatch(moveDown(figure.current, true))
                    //console.debug('--------------------------------------------------------')
                    break
                }
                default: {
                    break
                }
            }
        }
    }

    const handleStart = (e) => {
        dispatch(initField())
        dispatch(startGame(true))
    }

    return (
        <div 
            className={styles.container} 
            style={{ 
                width: `${cellWidth*dimentions[0]}px`, 
                height: `${cellHeight*dimentions[1]}px`
            }}
        >
            <>
                {field.map( (item, index) => {
                    const left = index * cellWidth
                    return item.map( (item1, index1) => {
                        const top = index1 * cellHeight
                        const color = COLORS[item1]
                        return (
                            <div
                                key={`${index}${index1}`}
                                className={styles.cell}
                                style={{
                                    left,
                                    top,
                                    backgroundColor: color,
                                    width: cellWidth,
                                    height: cellHeight,
                                }}
                            >
                                {useColorComponent(item1)}
                            </div>
                        )
                    })
                } )}
                {(!gameStart || gameOver) && <StartButton handleStart={handleStart}/>}
                {gameOver && <GameOver />}
                {gameStart && <PositionHelper />}
            </>
        </div>
    )
}
