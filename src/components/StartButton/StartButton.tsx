import React from 'react'

import styles from './styles.module.scss'

interface iStartButtonProps {
    handleStart: (e: React.SyntheticEvent) => void
}

export const StartButton: React.FC<iStartButtonProps> = ({ handleStart }) => {
    return (
        <div className={styles.container} onClick={handleStart}>
            {'Start Game'}
        </div>
    )
}
