import React from "react"
import { useAppSelector } from '../../state/hooks'

import { COLORS } from "../../constants"
import { getFigure } from "../../utils"
import { useColorComponent } from "../../hooks"

import styles from './styles.module.scss'

interface iFigureProps {
    figure: number
    color: number
}

export const Figure: React.FC<iFigureProps> = ({ figure: figureNumber, color: colorNumber }) => {
    const cellWidth = useAppSelector(state => state.field.cellWidth)
    const cellHeight = useAppSelector(state => state.field.cellHeight)

    const color = COLORS[colorNumber]
    const figure = getFigure(figureNumber)

    const transparentColor = 'rgba(255,255,255,0)'

    const makeFigure = () => {
        return figure.map( (item, index) => {
            let left = index*cellWidth
            return item.map( (item1, index1) => {
                // Отступ сверху 5 px
                let top = index1*cellHeight+5
                return (
                    <div
                        key={`${index}${index1}`}
                        className={styles.cell}
                        style={{
                            left,
                            top,
                            backgroundColor: item1 ? color : transparentColor,
                            width: `${cellWidth}px`,
                            height: `${cellHeight}px`,
                        }}
                    >
                        {useColorComponent(item1 ? colorNumber : 0)}
                    </div>
                )
            })
        })
    }

    return (
        <div className={styles.container}>
            {makeFigure()}
        </div>
    )
}
