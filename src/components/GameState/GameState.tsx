import React from "react"
import { useAppSelector } from '../../state/hooks'
import { Figure } from "../Figure/Figure"

import styles from './styles.module.scss'

export const GameState = () => {
    const cellWidth = useAppSelector(state => state.field.cellWidth)
    const score = useAppSelector(state => state.field.score)
    const next = useAppSelector(state => state.field.nextFigure)
    const color = useAppSelector(state => state.field.nextColor)
    const level = useAppSelector(state => state.field.level)
    const gameStart = useAppSelector(state => state.field.gameStart)

    return (
        <div className={styles.container}>
            <div className={`${styles.score} ${styles.block}`}>
                <span>Score</span>
                <span className={styles.info}>{score}</span>
            </div>
            <div className={`${styles.nextFigure}`} style={{ width: cellWidth*4 }}>
                <span>Next</span>
                {gameStart && <Figure figure={next} color={color} />}
            </div>
            <div className={`${styles.level} ${styles.block}`}>
                <span>Level</span>
                <span className={styles.info}>{level}</span>
            </div>
        </div>
    )
}
