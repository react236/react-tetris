import React from "react"
import { useAppSelector } from '../../state/hooks'

import { getFigure } from '../../utils'
import { COLORS } from "../../constants"

import styles from './styles.module.scss'

export const PositionHelper = () => {
    const x = useAppSelector(state => state.field.x)
    const figure = useAppSelector(state => state.field.figure)
    const colorNumber = useAppSelector(state => state.field.color)
    const cellWidth = useAppSelector(state => state.field.cellWidth)
    const rotate = useAppSelector(state => state.field.rotate)

    const figureArray = getFigure(figure)
    const figureLength = (rotate % 2) ? figureArray[0].length : figureArray.length
    const color = COLORS[colorNumber]
    
    return(
        <div className={styles.container} style={{ 
            backgroundColor: color,
            width: cellWidth*figureLength,
            left: cellWidth*x
        }}>
        </div>
    )
}
