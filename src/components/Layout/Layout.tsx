import React from "react"
import { useAppDispatch } from "../../state/hooks"

import { Field } from "../Field/Field"
import { GameState } from '../GameState/GameState'
import { initField } from "../../state/actions"

import styles from './styles.module.scss'

export const Layout = () => {
    const dispatch = useAppDispatch()

    dispatch(initField())

    return (
        <div className={styles.alignContainer}>
            <div className={styles.container}>
                <Field />
                <GameState />
            </div>
        </div>
    )
}
