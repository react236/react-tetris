export type tDimentions = [number, number]

export interface iCell {
    x: number
    y: number
    color: number
}

export type tFigure = [][]

// События
export type tEvent = 'UP' | 'DOWN' | 'LEFT' | 'RIGHT' | 'DROP'
