import React from 'react'
import { Provider } from 'react-redux'

import { store } from './state/store'
import { Layout } from './components/Layout/Layout'

function App() {
    return (
        <Provider store={store}>
            <Layout />
        </Provider>
    )
}

export default App;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
