import { COLORS_COUNT, FIGURES, FIGURES_COUNT } from "./constants"

export const getRandom = (max: number): number => {
    return Math.floor(Math.random() * max)
}

export const getNewFigure = (): number => {
    return getRandom(FIGURES_COUNT)
}

export const getFigure = (figure: number): number[][] => {
    return FIGURES[figure]
}

export const getNewColor = (): number => {
    const color = getRandom(COLORS_COUNT)
    if (color === 0) {
        return 1
    }

    return color
}

// Рассчитываем X, чтобы фигура была посередине
export const getMiddleX = (figureNumber: number, dimention: number): number => {
    return Math.floor( (dimention - getFigure(figureNumber).length) / 2 )
}
// Переворачиваем фигуру по часовой стрелке на 90 градусов
export const getRotatedFigure = (figure: number[][], rotation: number): any[] => {
    const figureElements = []

    switch (rotation) {
        case 0: {
            for(let i=0; i<figure.length; i++) {
                const line = []
                for(let j=0; j<figure[0].length; j++) {
                    line.push(figure[i][j])
                }
                figureElements.push(line)
            }
            break
        }
        case 1: {
            for(let i=0; i<figure[0].length; i++) {
                const line = []
                for(let j=figure.length-1; j>=0; j--) {
                    line.push(figure[j][i])
                }
                figureElements.push(line)
            }
            break
        }
        case 2: {
            for(let i=figure.length-1; i>=0; i--) {
                const line = []
                for(let j=figure[0].length-1; j>=0; j--) {
                    line.push(figure[i][j])
                }
                figureElements.push(line)
            }
            break
        }
        case 3: {
            for(let i=figure[0].length-1; i>=0; i--) {
                const line = []
                for(let j=0; j<figure.length; j++) {
                    line.push(figure[j][i])
                }
                figureElements.push(line)
            }
            break
        }
        default: {
            break
        }
    }

    return figureElements
}

// Длина перевернутой относительно текущей фигуры
export const rotatedLength = (figure: number[][]) => {
    return figure[0].length
}

// log field
export const logField = (field, dimentions, rows = 0) => {
    console.debug('Field: ------- ', dimentions[0]+' x '+dimentions[1])
    let rowsCount = rows ? rows : dimentions[1]
    
    for (let j=0; j<rowsCount; j++) {
        let str = ''
        for (let i=0; i<dimentions[0]; i++) {
            str = str + field[i][j] + ', '
        }
        console.debug(str)
    }
}
