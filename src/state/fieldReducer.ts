import { AnyAction } from 'redux'

import { getFigure, getMiddleX, getNewColor, getNewFigure, getRotatedFigure, logField } from '../utils'

import { 
    ADD_FIGURE,
    CLEAR_NEW_FIGURE,
    GAME_OVER,
    GET_EVENT,
    INIT_FIELD,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT,
    PUT_EVENT,
    REMOVE_FIGURE,
    ROTATE,
    START_GAME,
} from './constants'

import { SCORE_FOR_FIGURE, SCORE_FOR_LINE, SCORE_TO_LEVEL, SPEED_INCREMENT, SPEED_MAX } from '../constants'
import { tDimentions, tEvent } from '../interfaces'

interface iInitialState {
    field: number[][]
    dimentions: tDimentions
    rotate: number // Угол вращения фигуры
    figure: number // Номер фигуры
    nextFigure: number // Номер следующей фигуры
    color: number // Цвет текущей фигуры
    nextColor: number // Цвет следующей фигуры
    cellWidth: number // Ширина ячейки
    cellHeight: number // Высота ячейки
    x: number // Координата x левого верхнего угла фигуры
    y: number // Координата y левого верхнего угла фигуры
    newFigureFlag: boolean // Новая фигура на поле
    gameStart: boolean // Флаг что игра начата
    gameOver: false // Флаг что игра окончена
    speed: number // Задержка для setTimeout
    score: number // Очки
    level: number // Уровень
    eventQueue: tEvent[]
}

let busy: boolean = false // Флаг занятости
let scoreRange = SCORE_TO_LEVEL // Переменная для определения перехода на следующий уровень

// --- Инициализация поля ---
export const initField = (dimentions: tDimentions) => {
    // Инициализация поля
    const fieldArray = new Array<number[]>(dimentions[0]) // X

    for (let i=0; i<dimentions[0]; i++) {
        fieldArray[i] = new Array<number>(dimentions[1]) // Y
        for (let j=0; j<dimentions[1]; j++) {
            fieldArray[i][j] = 0
        }
    }

    return fieldArray
}

// Инициализация фигуры
const initFigure = () => {
    const figure = getNewFigure()
    const color = getNewColor()
    return { figure, color }
}

const initialDimentions: tDimentions = [10, 20]

const initialState: iInitialState = {
    field: [],
    dimentions: initialDimentions,
    rotate: 0,
    figure: 0,
    color: 0,
    nextFigure: 0,
    nextColor: 0,
    cellWidth: 30,
    cellHeight: 30,
    x: 0,
    y: 0,
    newFigureFlag: true,
    gameStart: false,
    gameOver: false,
    speed: 500,
    score: 0,
    level: 1,
    eventQueue: [],
}

export const fieldReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case INIT_FIELD: {
            //console.log('Init Field');

            const newState = Object.assign({}, initialState)
            const newField = initField(initialDimentions)
            const newFigure = initFigure()
            const nextFigure = initFigure()
            // Рассчитываем X, чтобы фигура была посередине
            const x = getMiddleX(newFigure.figure, state.dimentions[0])
            scoreRange = SCORE_TO_LEVEL
            busy = false

            return {
                ...newState,
                field: newField,
                figure: newFigure.figure,
                color: newFigure.color,
                nextFigure: nextFigure.figure, 
                nextColor: nextFigure.color,
                x,
            }
        }

        case ADD_FIGURE: {
            if (busy) {
                console.debug('Add busy')
                return { ...state }
            }
            busy = true
            console.debug('Add figure', action.payload.x, action.payload.y);

            const payload = action.payload
            // Проверяем, есть ли еще месть для фигур
            const figureWidth = payload.figure.length
            const figureHeight = payload.figure[0].length
            // Проверяем строку field[x][figureHeight] на наличие занятых клеток
            let isSpaceLeft = true
            //console.debug('state.x', state.x, 'figureWidth', figureWidth, 'figureHeight', figureHeight);
            for (let i=state.x; i<state.x+figureWidth; i++) {
                //console.debug('state.field[i][figureHeight]', state.field[i][figureHeight]);
                if (state.field[i][figureHeight] !== 0) {
                    isSpaceLeft = false
                    break
                }
            }
            
            if (!isSpaceLeft) {
                console.log('Game Over');
                
                // logField(state.field, state.dimentions, 8)
                return { ...state, gameOver: true }
            }

            // Добавляем фигуру на поле
            const newField = Object.assign([], state.field)
            for(let i=0; i<payload.figure.length; i++) {
                for(let j=0; j<payload.figure[0].length; j++) {
                    //console.log('x', payload.x, 'y', payload.y, 'i', i, 'j', j, 'payload.figure[i][j]', payload.figure[i][j]);

                    if (payload.figure[i][j] === 1) {
                        newField[payload.x+i][payload.y+j] = state.color //payload.color
                    }
                }
            }

            busy = false
            return { ...state, field: newField }
        }

        case REMOVE_FIGURE: {
            if (busy) {
                console.debug('Remove busy')
                return { ...state }
            }
            busy = true
            console.debug('Remove Figure', 'x', action.payload.x, 'y', action.payload.y, 'figure', state.figure);
            // logField(state.field, state.dimentions, 8)
            
            // Убираем фигуру с поля
            if (!state.newFigureFlag) {
                const newField2 = Object.assign([], state.field)
                for(let i=0; i<action.payload.width; i++) {
                    for(let j=0; j<action.payload.height; j++) {
                        //console.log('x', action.payload.x, 'y', action.payload.y, 'i', i, 'j', j, 'action.payload.figure[i][j]', action.payload.figure[i][j]);
                        
                        // Убираем только там, где есть цвет, чтобы не затронуть соседние фигуры
                        if (action.payload.figure[i][j] === 1) {
                            newField2[action.payload.x+i][action.payload.y+j] = 0
                        }
                    }
                }
                // console.debug('Figure removed');
                // logField(state.field, state.dimentions, 8)

                busy = false
                return { ...state, field: newField2, newFigureFlag: false }
            }
            // console.debug('Remove figure did nothing');
            // logField(state.field, state.dimentions, 8)

            busy = false
            return { ...state }
        }

        case ROTATE: {
            if (busy) {
                console.debug('Rotate busy')
                return { ...state }
            }
            busy = true
            console.debug('Rotate');
            // logField(state.field, state.dimentions, 8)

            let rotate = state.rotate+1
            let x = state.x
            if (rotate > 3) rotate = 0
            
            const figure = getRotatedFigure(getFigure(state.figure), rotate)
            // Проверяем, не упираемся ли в правую границу
            const dif = state.dimentions[0] - (x+figure.length)
            if (dif < 0) {
                x = x+dif
            }

            // Проверяем, не будет ли пересекаться фигура с другими фигурами после вращения
            let collisionFlag = false
            for (let i=0; i<figure.length; i++) {
                for (let j=0; j<figure[0].length; j++) {
                    if (figure[i][j] === 1) {
                        if (state.field[x+i][state.y+j] !== 0) {
                            // console.debug('x', x, 'y', state.y, 'i', i, 'j', j, 'figure[i][j]', figure[i][j], 'field[x+i][y+j]', state.field[state.x+i][state.y+j])
                            collisionFlag = true
                            break
                        }
                    }
                }
                if (collisionFlag) break
            }

            if (collisionFlag) {
                // console.debug('Rotate Collision');
                busy = false
                return { ...state }
            }

            busy = false
            return { ...state, x, rotate }
        }

        case MOVE_DOWN: {
            if (busy) {
                console.debug('Down busy')
                return { ...state }
            }
            busy = true
            console.debug('Move down')

            let x = state.x
            let y = state.y
            const figure = action.payload.figure
            const newField = Object.assign([], state.field)
            let color = state.color
            let figureNumber = state.figure
            let score = state.score
            let level = state.level
            let speed = state.speed

            // Проверяем можно ли дальше опускать
            let collisionFlag = false

            do {
                y = y+1

                // 1. Проверяем, не достигли ли дна
                // console.debug('Check bottom');
                if (y+figure[0].length > initialDimentions[1]) {
                    collisionFlag = true
                }

                // 2. Проверяем фигуру по ее по столбцам (Y) снизу вверх, не упираемся ли в другую фигуру
                if (!collisionFlag) {
                    for(let i=0; i<figure.length; i++) {
                        for(let j=figure[0].length-1; j>=0; j--) {
                            if (figure[i][j] === 1) {
                                if (state.field[x+i][y+j] !== 0) {
                                    collisionFlag = true
                                }
                                break
                            }
                        }
                        if (collisionFlag) break
                    }
                }

            } while (!collisionFlag && action.payload.auto)

            // 3. Если достигли дна или уперлись в фигуру, фиксируем текущую фигуру
            if (collisionFlag) {
                // console.debug('Down collision');

                y = y-1
                // Копируем фигуру на поле
                for(let i=0; i<figure.length; i++) {
                    for(let j=0; j<figure[0].length; j++) {
                        // Меняем только фактически занимаемое фиругой место
                        if (figure[i][j] === 1) {
                            newField[x+i][y+j] = color
                        }
                        // console.debug(i, j, newField[x+i][y+j])
                    }
                }
                score += SCORE_FOR_FIGURE

                // Переставляем следующую фигуру на место текущей, и инициализируем следующую
                figureNumber = state.nextFigure
                color = state.nextColor
                const { figure: nextFigure, color: nextColor } = initFigure()

                x = getMiddleX(figureNumber, state.dimentions[0])
                y = 0

                // Проверяем, есть ли сплошные заполненные строки, и убираем их
                let fillRowFlag = false
                for (let j=0; j<state.dimentions[1]; j++) {
                    fillRowFlag = true
                    for (let i=0; i<state.dimentions[0]; i++) {
                        if (newField[i][j] === 0) {
                            fillRowFlag = false
                            break
                        }
                    }
                    if (fillRowFlag) {
                        // Удаляем заполненную строку и добавляем пустую сверху
                        for (let i=0; i<state.dimentions[0]; i++) {
                            newField[i].splice(j, 1)
                            newField[i].unshift(0)
                        }
                        fillRowFlag = false
                        score += SCORE_FOR_LINE
                    }
                }

                // Установка очков и уровня
                if (score >= scoreRange) {
                    level += 1
                    if (speed >= SPEED_MAX) {
                        speed -= SPEED_INCREMENT
                    }
                    scoreRange += SCORE_TO_LEVEL
                }

                busy = false
                return {
                    ...state,
                    field: newField,
                    newFigureFlag: true,
                    figure: figureNumber,
                    x,
                    y,
                    color,
                    score,
                    level,
                    speed,
                    nextFigure,
                    nextColor
                }
            }

            busy = false
            return { ...state, y }
        }

        case MOVE_LEFT: {
            if (busy) return { ...state }
            busy = true
            // console.debug('Move left')

            let x = state.x-1
            // Проверяем границу поля
            if (x < 0) {
                busy = false
                return { ...state }
            }
            // Проверяем, нет ли уже других фигур слева
            // Проверяем по строкам (X) слева направо, не упираемся ли в другую фигуру
            let collisionFlag = false
            const figure = getRotatedFigure(getFigure(state.figure), state.rotate)
            for (let j=0; j<figure[0].length; j++) {
                for (let i=0; i<figure.length; i++) {
                    if (figure[i][j] === 1) {
                        if (state.field[x+i][state.y+j] !== 0) {
                            collisionFlag = true
                        }
                        break
                    }
                }
                if (collisionFlag) break
            }
            // Если упираемся в фигуру, откатываемся назад
            if (collisionFlag) {
                x = x+1
            }
            busy = false
            return { ...state, x }
        }

        case MOVE_RIGHT: {
            if (busy) return { ...state }
            busy = true
            // console.debug('Move right')

            let x = state.x + 1
            if (x+action.payload > initialDimentions[0]) {
                busy = false
                return { ...state }
            }
            // Проверяем, нет ли уже других фигур справа
            // Проверяем по строкам (X) справа налево, не упираемся ли в другую фигуру
            let collisionFlag = false
            const figure = getRotatedFigure(getFigure(state.figure), state.rotate)
            for (let j=0; j<figure[0].length; j++) {
                for (let i=figure.length-1; i>=0; i--) {
                    if (figure[i][j] === 1) {
                        if (state.field[x+i][state.y+j] !==0) {
                            collisionFlag = true
                        }
                        break
                    }
                }
                if (collisionFlag) break
            }
            // Если упираемся в фигуру, откатываемся назад
            if (collisionFlag) x = x-1
            busy = false
            return { ...state, x }
        }

        case CLEAR_NEW_FIGURE: {
            return { ...state, newFigureFlag: false }
        }

        case START_GAME: return { ...state, gameStart: true }

        case GAME_OVER: return { ...state, gameOver: true }

        case PUT_EVENT: {
            const newEventQueue = Object.assign([], state.eventQueue)
            newEventQueue.push(action.payload)
            return { ...state, eventQueue: newEventQueue }
        }

        case GET_EVENT: {
            // Просто убираем событие из очереди
            const newEventQueue = Object.assign([], state.eventQueue)
            newEventQueue.shift()
            return { ...state, eventQueue: newEventQueue }
        }

        default: return state
    }
}
