import { AnyAction } from "redux"

import {  } from './constants'

interface appState {

}

const initialState: appState = {

}

export const appReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        default: return state
    }
}
