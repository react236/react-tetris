import { AnyAction } from 'redux'

import { store } from './store'
import { 
    INIT_FIELD,
    ADD_FIGURE,
    ROTATE,
    REMOVE_FIGURE,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT,
    CLEAR_NEW_FIGURE,
    START_GAME,
    GAME_OVER,
    PUT_EVENT,
    GET_EVENT,
} from './constants'
import { tEvent, tFigure } from '../interfaces'

export function initField(): AnyAction {
    //store.dispatch(initMinesLeft())
    //store.dispatch(setGameWon(false))
    return { type: INIT_FIELD }
}

export function addFigure( figure: tFigure, x: number, y: number, color: number ): AnyAction {
    return { type: ADD_FIGURE, payload: { figure, x, y, color } }
}

export function removeFigure( figure: tFigure, x: number, y: number, width: number, height: number ): AnyAction {
    return { type: REMOVE_FIGURE, payload: { figure, x, y, width, height } }
}

export function rotate( len: number ): AnyAction {
    return { type: ROTATE, payload: len }
}

export function moveDown( figure: tFigure, auto: boolean ): AnyAction {
    return { type: MOVE_DOWN, payload: { figure, auto } }
}

export function moveLeft(): AnyAction {
    return { type: MOVE_LEFT }
}

export function moveRight( len: number ): AnyAction {
    return { type: MOVE_RIGHT, payload: len }
}

export function clearNewFigure(): AnyAction {
    return { type: CLEAR_NEW_FIGURE }
}

export function startGame( value: boolean): AnyAction {
    return { type: START_GAME, payload: value }
}

export function gameOver( value: boolean ): AnyAction {
    return { type: GAME_OVER, payload: value }
}

export function putEvent( event: tEvent ): AnyAction {
    return { type: PUT_EVENT, payload: event }
}

export function getEvent(): AnyAction {
    return { type: GET_EVENT }
}
