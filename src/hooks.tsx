import React from 'react'

import Red from './img/red.svg'
import Green from './img/green.svg'
import Yellow from './img/yellow.svg'
import Blue from './img/blue.svg'
import Orange from './img/orange.svg'
import Magenta from './img/magenta.svg'

export const useColorComponent = (color: number) => {
    switch (color) {
        case 0: return null
        case 1: return <Red />
        case 2: return <Green />
        case 3: return <Yellow />
        case 4: return <Blue />
        case 5: return <Orange />
        case 6: return <Magenta />
        default: return <div>K</div>
    }
}
