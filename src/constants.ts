export const FIGURES = [
    [ [1,0], [1,1], [0,1] ],
    [ [0,1], [1,1], [1,0] ],
    [ [1,1], [1,1] ],
    [ [1,1], [0,1], [0,1] ],
    [ [0,1], [0,1], [1,1] ],
    [ [0,1], [1,1], [0,1] ],
    [ [1], [1], [1], [1] ],
]

export const FIGURES_COUNT = FIGURES.length

export const COLORS: string[] = ['transparent', 'red', 'green', 'yellow', 'blue', 'orange', 'magenta']

export const COLORS_COUNT = COLORS.length

// Сколько очков нужно набрать для увеличения уровня
export const SCORE_TO_LEVEL = 20

// Сколько очков за фигуру
export const SCORE_FOR_FIGURE = 1

// Сколько очков за линию
export const SCORE_FOR_LINE = 5

export const SPEED_INCREMENT = 10

// Максимальная скорость
export const SPEED_MAX = 220
